public class secondLargestInArray {

    static int secondLargest(int[] arr) {

        int largest = Integer.MIN_VALUE;
        int secondLargest = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (largest < arr[i]) {
                secondLargest = largest;
                largest = arr[i];
            } else if(secondLargest < arr[i]) {
                secondLargest = arr[i];
            }
        }

        return secondLargest;
    }
}
