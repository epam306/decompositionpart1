public class GDTfor4 {

    static int greatestCommonDivisor(int a, int b, int c, int d){

        int smallest = Math.min(Math.min(a,b),Math.min(a,b));

        int gdt = 0;

        for (int i = smallest / 2; i > 0; i--) {
            if (a % i == 0 && b % i == 0) {
                gdt = i;
                break;
            }
        }

        return gdt;
    }
}
