public class LCM3 {

    static int lowestCommonMultiple(int a, int b, int c){

       int max = Math.max(Math.max(a,b),c);

        while(true){

            if(max % a == 0 && max % b == 0 && max % c == 0) {
                return max;
            }
            ++max;
        }
    }
}
