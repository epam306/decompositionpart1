public class oddNumbers {

    static int sumOfFactorials () {

        int sum = 1;
        int multiplication = 1;
        for (int i = 1; i < 10; i++){


            if(i % 2 == 1 && i > 1) {

                multiplication *= (i * (i-1));

                 sum += multiplication;
            }

        }

        return sum;
    }
}
