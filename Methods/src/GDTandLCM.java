class GDTandLCM {

    static int greatestCommonDivisor(int a, int b) {

        int gdt = 0;
        int smallest;

        smallest = a < b ? a : b;

        if (a % b == 0 || b % a == 0) {
            return smallest;
        }

        for (int i = smallest / 2; i > 0; i--) {
            if (a % i == 0 && b % i == 0) {
                gdt = i;
                break;
            }
        }

        return gdt;
    }

    static int leastCommonMultiple(int a, int b) {
        return (a * b) / greatestCommonDivisor(a, b);
    }
}